# BeatBuddy

Beat buddy aims to provide a fun way to share a playlist with friends or coworkers !
Log in, select your playlist, and vote on the currently playing song

## Running locally

To run this project locally, ensure you have the following installed :
- `git`
- `docker` and / or `nodejs`

Start by cloning this project, then in the generated folder, follow one of the two methods bellow

### node
This allows for a hot refresh of your application as you code. It requires a recent version of nodejs on your machine

1. `npm i` to install the dependencies
2. `npm run start:dev` to start the project

### docker
This allows for a production build identical to the one generated in the CI of this project. It requires a recent version of docker on your machine, but installing nodejs isn't necessary.

1. `docker build -t beatbuddy .`
2. `docker run -p 3000:3000 beatbuddy`

## Contributing

Not yet

## License

MIT - Copyright 2023 BeatBuddy

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
