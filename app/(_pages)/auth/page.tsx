export default function Auth() {
  return (
    <>
      <main className="w-screen h-screen flex justify-center items-center bg-slate-200 text-slate-700 pattern">
        <section className="h-max w-80 flex flex-col bg-slate-50 rounded-xl justyify-center items-center p-4 py-8 gap-4">
          <h1 className="my-4 text-2xl">BeatBuddy</h1>

          <div className="w-4/5 flex flex-col">
            <label htmlFor="username">Username :</label>
            <input type="text" id="username" className="border border-slate-200 bg-white rounded-lg py-1 px-2 focus:border-orange-400 focus:outline-none" />
          </div>

          <div className="w-4/5 flex flex-col">
            <label htmlFor="password">Password :</label>
            <input type="password" id="password" className="border border-slate-200 bg-white rounded-lg py-1 px-2 focus:border-orange-400 focus:outline-none" />
          </div>

          <button className="bg-orange-400 py-2 px-3 w-4/5 rounded-lg text-white my-4 transition-colors duration-150 hover:bg-orange-600">Login !</button>

          <p>No account yet ?</p>
          <button className="bg-white border border-orange-400 border-inset py-2 px-3 w-4/5 rounded-lg text-orange-400 transition-colors duration-150 hover:bg-orange-600 hover:border-orange-600 hover:text-white">Sign in instead</button>
        </section>
      </main>
    </>
  )
}