import { createContext, useContext, useState } from "react";

const AuthContext = createContext({isAuthenticated: false, setIsAuthenticated: (isAuthenticated: boolean) => {}});

export type AuthProviderValue = {
  isAuthenticated: boolean;
  setIsAuthenticated: (isAuthenticated: boolean) => void;
}

export function useAuth() {
  return useContext(AuthContext);
}

export function AuthProvider({ children }: { children: React.ReactNode }) {
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  const value: AuthProviderValue = {
    isAuthenticated,
    setIsAuthenticated
  };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}