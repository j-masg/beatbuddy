'use client';

import { useRouter } from "next/navigation"
import { useEffect } from "react";

import { AuthProvider, useAuth } from "./_providers/auth.provider";


export default function Home() {
  const { isAuthenticated } = useAuth();
  const router = useRouter();

  useEffect(() => {
    if (!isAuthenticated) {
      router.push("/auth");
    }
  }, [router, isAuthenticated]);

  return (
    <AuthProvider>
      <main>
        
      </main>
    </AuthProvider>
  )
}
