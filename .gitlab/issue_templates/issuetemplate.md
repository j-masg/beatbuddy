## Summary

(summarize the bug you encountered or feature you would like to see implemented in a cincise manner)

## Steps to reproduce

(How one can reproduce the issue. You can skip this step if your issue is a feature proposition rather than a bug)

## What is the current behavior ?

(How the bug actually manifests, or how the application behaves currently)

## What is the expected behavior ?

(Describe what should happen instead of what currently is)

## Relevant logs or screenshots

(Anything that would facilitate or work is welcome in this section !)

## Proposed fix or implementation

(If you can, provide a code sample and / or link to the relevant part of the project)

(Please remove irrelevant labels)
/labels ~BUG ~FEATURE ~DISCUSSION